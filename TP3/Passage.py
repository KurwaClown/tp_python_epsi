from Caisse import Caisse
from errors import *
import sys

class Passage(object):
    
    def __init__(self, caisse):
        self.currentCheckout = caisse
        self.nbProduct = 0;
        
        
    def getProductNB(self):
        print("Combien de produits voulez-vous acheter ?")
        return int(input())

    def getProductID(self):
        #Saisis de l'id du produit
        print("Veuillez saisir l'id du produit souhaité : ")
        try:
            id = int(input())
            if id < 1 or id > 4:
                raise IdNotInRange(id)
            return id;
        except ValueError as error:
            sys.exit("Mauvaise donnée saisis, arrêt du programme")
        except IdNotInRange as error:
            sys.exit(error)

    def getProductQuantity(self):
        #Saisis de la quantitée souhaitée
        print("Veuillez saisir la quantité du produits souhaité")
        try:
            quantity = int(input())
            if quantity < 1 :
                raise QuantityError
            return quantity
        except ValueError as error:
            sys.exit("Mauvaise donnée saisis, arrêt du programme")
        except QuantityError as error:
            sys.exit(error)


    def initiatePassage(self):
        self.currentCheckout.showAvailableFruits()
        nbProduct = self.getProductNB()
        for i in range(0, nbProduct):
            idProduct = self.getProductID()
            quantityProduct = self.getProductQuantity()
            self.currentCheckout.addProduct(idProduct, quantityProduct)
        self.currentCheckout.pay()
