#Séparateur et titre du tableau avec les ID
IDSEPARATOR = "+----------+---------------------------------+----------+"
IDFIELDNAME = "|    ID    |               Nom               |   Prix   |"

#Séparateur et titre du tableau avec les produits acheter
HTSEPARATOR = "+---------------------------------+----------+------------------------+----------------+"
HTFIELDNAME = "|               Nom               |   Prix   |        Quantité        |    Total HT    |"

FRUITS  = {1:{"nom": "Banane", "prix" : 4.0}, 2:{"nom": "Pomme", "prix" : 2.0}, 3:{"nom": "Orange","prix" : 1.5}, 4:{"nom": "Poire", "prix" : 3.0}}

class Caisse(object):


    def __init__(self):
        self.totalCashedIn = 0
        self.totalHT = 0
        self.totalTTC = 0
        self.reduction = False
        self.basket = []

    def addProduct(self, fruitId, quantity):
        fruit = FRUITS[fruitId]['nom']
        price = FRUITS[fruitId]['prix']
        self.basket.append([fruit, price, quantity])
        self.totalHT += price*quantity
        self.totalTTC = self.totalHT*1.2
        
    def pay(self):
        if self.totalTTC>200:
            self.reduction = True
        self.showBasket()
        self.addCashedIn()
        self.emptyBasket()
        
    def showBasket(self):
        fruitHTTable = "{}\n{}\n{}\n".format(HTSEPARATOR, HTFIELDNAME, HTSEPARATOR)

        for fruit in self.basket:
            fruitHTTable +="|{}                           |{}       |{}                      |{}           |\n".format(fruit[0], fruit[1],fruit[2], fruit[1]*fruit[2])

        fruitHTTable += HTSEPARATOR;

        print(fruitHTTable)
        if self.reduction:
            print("Sous-Total HT : {}\nRemise 5% : {}\nTotal HT : {}\nTotal TTC : {}".format(self.totalHT, self.totalHT*0.05,self.totalHT*0.95 , self.totalTTC*0.95))
        else:
            print("Total HT : {}\nTotal TTC : {}".format(self.totalHT , self.totalTTC))

    def showAvailableFruits(self):
        fruitIDTable = "{}\n{}\n{}\n".format(IDSEPARATOR, IDFIELDNAME, IDSEPARATOR)

        for fruit in FRUITS:
            fruitIDTable +="|{}         |{}                           |{}       |\n".format(fruit, FRUITS[fruit]['nom'],FRUITS[fruit]['prix'])
        fruitIDTable += IDSEPARATOR;

        print(fruitIDTable)

    def emptyBasket(self):
        self.totalHT = 0
        self.totalTTC = 0
        self.reduction = False
        self.basket = []
        
    def addCashedIn(self):
        if self.reduction:
            self.totalCashedIn += self.totalTTC*0.95
        else:
            self.totalCashedIn += self.totalTTC

    
        