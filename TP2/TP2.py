#Classe comportant les erreurs d'exceptions
from errors import *

import sys

#Séparateur et titre du tableau avec les ID
IDSEPARATOR = "+----------+---------------------------------+----------+"
IDFIELDNAME = "|    ID    |               Nom               |   Prix   |"

#Séparateur et titre du tableau avec les produits acheter
HTSEPARATOR = "+---------------------------------+----------+------------------------+----------------+"
HTFIELDNAME = "|               Nom               |   Prix   |        Quantité        |    Total HT    |"

TVA = 1.2

fruitsIDs = [[1, "Banane", 4.0], [2, "Pomme", 2.0], [3, "Orange", 1.5], [4, "Poire", 3.0]]


#Fonction pour afficher les fruits disponibles
def showAvailableFruits():
    fruitIDTable = "{}\n{}\n{}\n".format(IDSEPARATOR, IDFIELDNAME, IDSEPARATOR)

    for fruit in fruitsIDs:
        fruitIDTable +="|{}         |{}                           |{}       |\n".format(fruit[0], fruit[1],fruit[2])
    fruitIDTable += IDSEPARATOR;

    print(fruitIDTable)

#Fonction pour afficher les fruits achetés
def showBoughtFruits(fruitsBought):
    fruitHTTable = "{}\n{}\n{}\n".format(HTSEPARATOR, HTFIELDNAME, HTSEPARATOR)
    fruitsHT = 0

    for fruit in fruitsBought:
        fruitHTTable +="|{}                           |{}       |{}                      |{}           |\n".format(fruit[0], fruit[1],fruit[2], fruit[3])
        fruitsHT += fruit[3]

    fruitHTTable += HTSEPARATOR;
    fruitsTTC = fruitsHT*TVA

    reduction = 0;
    if fruitsTTC>200:
            reduction = 0.05*fruitsTTC

    #Afficher les résultats de l'achat suivant si une réduction a été appliquée ou non
    print(fruitHTTable)
    if reduction == 0:
        print("Total HT : {}\nTotal TTC : {}".format(fruitsHT , fruitsTTC))
    else:
        print("Sous-Total HT : {}\nRemise 5% : {}\nTotal HT : {}\nTotal TTC : {}".format(fruitsHT, reduction,fruitsHT-reduction , fruitsTTC))

#Fonction pour demander à l'utilisateur quels fruits il veux acheter
def getFruits():
    fruitsBought = []

    print("Bonjour, combiens de produits différents seront à saisir ?")

    try:
        productCount = int(input())
        if productCount < 1:
            print("Le minimum étant de un seul produits, vous ne pourrez saisir qu'un seul produit !")
            productCount = 1
    except ValueError as error:
        print("Mauvaise donnée saisis, vous ne pourrez saisir qu'un seul produit !")
        productCount = 1

    for i in range(0,productCount):
        
        #Saisis de l'id du produit
        print("Veuillez saisir l'id du produit {} : ".format(i+1))
        try:
            id = int(input())
            if id < 1 or id > 4:
                raise IdNotInRange(id)
            fruitsBought.append(fruitsIDs[id-1])
            currentFruit = fruitsBought[i]
            currentFruit.pop(0)
        except ValueError as error:
            sys.exit("Mauvaise donnée saisis, arrêt du programme")
        except IdNotInRange as error:
            sys.exit(error)

        #Saisis de la quantitée souhaitée
        print("Veuillez saisir la quantité de produits {}: ".format(i+1))
        try:
            quantity = int(input())
            if quantity < 1 :
                raise QuantityError
            currentFruit.append(quantity)
        except ValueError as error:
            sys.exit("Mauvaise donnée saisis, arrêt du programme")
        except QuantityError as error:
            sys.exit(error)

        currentFruit.append(currentFruit[1] * currentFruit[2])

    return fruitsBought


#Fonction main
if __name__ == "__main__":
    showAvailableFruits()
    
    fruits = getFruits()
    showBoughtFruits(fruits)