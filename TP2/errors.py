#Erreur produite quand l'id saisis n'est pas dans la plage disponible
class IdNotInRange(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]

    def __str__(self):
        if self.message:
            return "L'id {0} n'est pas dans la plage proposée  ".format(self.message)

#Erreur produite quand la quantité est nulle ou négative
class QuantityError(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]

    def __str__(self):
        return "Le prix ne peut pas être inferieur à 1, arrêt du programme"