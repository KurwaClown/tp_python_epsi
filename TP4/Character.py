class Character(object):
    def __init__(self):
        self.hp = 75


    def move(self, direction):
        directionDict = {
            0 : "Up",
            1 : "Right",
            2 : "Down",
            3 : "Left"
        }
        print("You decided to go {}".format(directionDict[direction]))

    def loseHP(self, hitDamage):
        self.hp -= hitDamage
        if self.hp<1:
            return
        else:
            print("You now have {}HP".format(self.hp))
    
    def healHP(self, healStrengh):
        self.hp += healStrengh
        if self.hp>75:
            self.hp=75
            print("You're full health")
        else:
            print("You now have {}HP".format(self.hp))

    def getHP(self):
        return self.hp