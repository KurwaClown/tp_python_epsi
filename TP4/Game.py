import Room
import Character
import random as rd
from os import system
from sys import exit
import asyncio
import time
import msvcrt

class Game(object):
    def __init__(self):
        self.player = Character.Character()
        self.castle = Room.Room(self.player)
        self.exitChance = 0.75

    def startGame(self):
        print("You wake up in a prison cell, with the door half open.")
        userInput = input("Go out (y,n) : ").lower()
        if userInput == 'y':
            self.Controller()
        elif userInput =='n':
            self.gameEnd(4)
        else:
            self.gameEnd(2)
        
        

    def Controller(self):
        lastDirection = {
            0 : 2,
            1 : 3,
            2 : 0,
            3 : 1
        }
        direction = rd.randint(0,3)
        self.player.move(direction)
        while (rd.randint(1,100)>=self.exitChance):
            possibilities = [i for i in range(0,4) if i not in [lastDirection[direction]]]
            try:
                userInput = int(input("There's 3 choice of direction, which one do you take (1,2,3) ? "))-1
            except ValueError as e:
                self.gameEnd(5)
            system('cls')
            try:
                self.player.move(possibilities[userInput])
                self.castle.newRoom()
            except IndexError as e:
                self.gameEnd(0)
            if self.player.getHP() <1:
                self.gameEnd(3)
            self.exitChance +=0.75
        self.gameEnd(4)
            

    def gameEnd(self, reasonId):
        userEvent = False
        reason = {
            0: "You pushed a hidden button while trying to find a secret door.\nAs you push the button, two dart coated in a deadly poison hit you in the back. You life faded away in less than a second ",
            1: "You stayed here for eternity and died",
            2: "You didn't manage to choose and died of old age",
            3: "You got attacked and died",
            4: "You see the sunlight coming out of a door. You open the door and go out.\nAs you set a foot out, the entrance guard, knocks you out and take you back to your cell. This time definitly closed",
            5: "As you enter the room some glowing words on the wall are catching your eyes\nAs you read them out loud, the floor start rising up until you get flatten out by the roof."
        }
        position = 0
        endLen = len(reason[reasonId])-1

        if reason==4:
            userEvent = asyncio.run(self.corout())
        while not msvcrt.kbhit() and position <= endLen:
            print(reason[reasonId][position], end='', flush=True)
            time.sleep(0.03)
            position+=1
        if msvcrt.kbhit():
            for i in range(4):
                print('.', end='', flush=True)
            time.sleep(1)
            print("\n\nYou hit entrance the guard before he gets you and run away. GJ you save yourself")
        exit()

    
