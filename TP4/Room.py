import random
import Character

class Room(object):
    def __init__(self, char, roomTypeId = 0):
        self.roomTypeId = roomTypeId
        self.char = char

    def newRoom(self):
        self.roomTypeId = random.randint(0,2)
        self.getRoomType()

    def getRoomType(self):
        roomType = {
            0: self.emptyRoom,
            1: self.healingRoom,
            2: self.fightRoom
        }
    
        roomType[self.roomTypeId]()

    def emptyRoom(self):
        print("You're in an empty room !")

    def healingRoom(self):
        healingEvent= {
            0: "There is a rejuvenating fountain in this room !",
            1: "You find a health potion on the ground as you enter the room and drink it"
        }
        print(healingEvent[random.randint(0,1)])
        healingStrenght = random.randint(1,10)
        print("You drink it and get {}HP back".format(healingStrenght))
        self.char.healHP(healingStrenght)

    def fightRoom(self):
        print("There is an ennemy in this room !")
        hitDamage = random.randint(10,25)
        print("You beat him but lose {}HP".format(hitDamage))
        self.char.loseHP(hitDamage)