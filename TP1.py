TVA = 1.2

print("Bonjour, combiens de produits différents seront à saisir ?")

try:
    productCount = int(input())
    if productCount < 1:
        print("Le minimum étant de un seul produits, vous ne pourrez saisir qu'un seul produit !")
        break
except ValueError as error:
    print("Mauvaise donnée saisis, vous ne pourrez saisir qu'un seul produit !")
    productCount = 1

for i in range(0,productCount):
    print("Veuillez saisir le prix hors taxe du produit {} : ".format(i+1))
    try:
        priceHT = float(input())
        if priceHT < 1 :
            print("Le prix ne peut pas être inferieur à 1, arrêt du programme")
            break
    except ValueError as error:
        print("Mauvaise donnée saisis, arrêt du programme")
        break
    

    print("Veuillez saisir la quantité de produits {}: ".format(i+1))
    try:
        quantity = int(input())
        if quantity < 1 :
            print("La quantité ne peut pas être inferieur à 1, arrêt du programme")
            break
    except ValueError as error:
        print("Mauvaise donnée saisis, arrêt du programme")
        break
    

    priceTVA = priceHT*quantity*TVA

    if priceTVA>200:
        priceTVA *= 0.95
        print("Avec la reduction de 5%, le montant s'élève à {}".format(priceTVA))
    else:
        print("Le montant s'élève à {}".format(priceTVA))